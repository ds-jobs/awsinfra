import {searchRest} from "../../../src/handlers/search-rest";
import SearchRequestDto from "../../../src/classes/SearchRequestDto";
// @ts-ignore
import {constructAPIGwEvent} from "../../utils/helpers";
import {SearchResponseDto} from "../../../src/classes/SearchResponseDto";
import {HotelDto} from "../../../src/classes/HotelDto";

// This includes all tests for getSearchRest()
describe('Test getSearchRest', () => {

    it('should return searchResponseDto', async () => {
        const event = constructAPIGwEvent(
            <SearchRequestDto>{
                where: 'Milano',
                fromDate: '2021-05-20T12:07:40.041Z',
                toDate: '2021-06-20T12:07:40.041Z'
            }, {
                method: 'GET',
                path: 'searchRest'
            });

        const result = await searchRest(event);

        const expectedResult = {
            statusCode: 200,
            body: JSON.stringify(<SearchResponseDto>{
                hotels: [
                    <HotelDto>{
                        name: '',
                        address: '',
                        description: '',
                        price: 111,
                        stars: 4
                    }
                ]
            })
        };

        expect(result).toEqual(expectedResult);
    });
}); 
