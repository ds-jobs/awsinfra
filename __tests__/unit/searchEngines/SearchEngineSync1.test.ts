import SearchEngine from "../../../src/searchEngines/SearchEngine";
import {HotelDto} from "../../../src/classes/HotelDto";
import DoneCallback = jest.DoneCallback;

describe('Test SearchEngine', () => {

    it('should return HotelDto[]', (done: DoneCallback) => {
        const searchEngine = new SearchEngine()

        searchEngine.search({
            where: 'Milano',
            fromDate: '01-01-2021',
            toDate: '31-01-2021'
        }).subscribe((value: HotelDto[]) => {
            expect(value.length).toBeGreaterThan(0)
            expect(value).toEqual([{
                "address": "",
                "description": "",
                "name": "",
                "price": 111,
                "stars": 4,
            }])

        }, done())
    });
});
