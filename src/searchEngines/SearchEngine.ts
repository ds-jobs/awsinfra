import {SearchEngineInterface} from "./SearchEngineInterface";
import SearchRequestDto from "../classes/SearchRequestDto";
import {Observable} from 'rxjs';
import {HotelDto} from "../classes/HotelDto";

export default class SearchEngine implements SearchEngineInterface {
    constructor() {
    }

    search(searchRequest: SearchRequestDto): Observable<HotelDto[]> {
        let wait = 0
        return new Observable<HotelDto[]>(subscriber => {


            for (let i = 0; i <= (Math.random() * 30); i++) {
                const ms = Math.random() * 1000 * i;
                wait += ms;
                const hotel = <HotelDto>{
                    name: i.toString(),
                    address: searchRequest.where,
                    description: '',
                    price: parseFloat((Math.random() * 100).toFixed(2)),
                    stars: parseInt((Math.random() * 5).toFixed(0))
                };
                setTimeout(() => subscriber.next([hotel, hotel]), ms)
            }
            setTimeout(() => subscriber.complete(), wait + 200)
        })
    }

}