import {SearchEngineInterface} from "./SearchEngineInterface";
import SearchRequestDto from "../classes/SearchRequestDto";
import {HotelDto} from "../classes/HotelDto";
import {Observable} from 'rxjs';

export default class SearchEnginesHandler {
    private searchEngines: SearchEngineInterface[];

    constructor(searchEngines: SearchEngineInterface[]) {
        this.searchEngines = searchEngines;
    }

    executeSearch(searchRequest: SearchRequestDto): Observable<HotelDto[]> {
        return new Observable<HotelDto[]>(subscriber => {
            Promise.all(
                this.searchEngines.map(searchEngine =>
                    new Promise((resolve) => searchEngine.search(searchRequest).subscribe({
                        next: (hotels: HotelDto[]) => subscriber.next(hotels),
                        error: () => {
                        },
                        complete: () => resolve(null)
                    }))
                ))
                .then(() => subscriber.complete())
        })
    }
}