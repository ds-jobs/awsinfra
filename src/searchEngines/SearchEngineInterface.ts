import SearchRequestDto from "../classes/SearchRequestDto";
import {HotelDto} from "../classes/HotelDto";
import {Observable} from 'rxjs';

export interface SearchEngineInterface {
    search(searchRequest: SearchRequestDto): Observable<HotelDto[]>;
}