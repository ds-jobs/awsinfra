'use strict';
import express from 'express';
import http from 'http';
import {Server} from 'socket.io';
import {searchRest} from "./handlers/search-rest";
import {searchSocket} from "./handlers/search-socket";
import bodyParser from "body-parser";

const app = express();

const server = http.createServer(app);

const io = new Server(server);

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.json())

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/assets/html/search.html');
});
app.post('/searchRest', searchRest)
io.on('connection', (socket) => {
    socket.on('searchsocket', (message) => {
        searchSocket(message).subscribe(searchresponse => io.to(socket.id).emit('searchsocketresult', searchresponse))
    });
});

server.listen(3000, () => {
    console.log('listening on *:3000');
});
