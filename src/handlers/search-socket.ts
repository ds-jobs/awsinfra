import {SearchResponseDto} from "../classes/SearchResponseDto";
import SearchEnginesHandler from "../searchEngines/SearchEnginesHandler";
import SearchEngine from "../searchEngines/SearchEngine";
import {HotelDto} from "../classes/HotelDto";
import {Observable} from "rxjs";

export const searchSocket = (searchRequest): Observable<SearchResponseDto> => {
    const searchEnginesHandler = new SearchEnginesHandler([
        new SearchEngine(),
        new SearchEngine(),
        new SearchEngine(),
        new SearchEngine()
    ])
    return new Observable<SearchResponseDto>(subscriber =>
        searchEnginesHandler.executeSearch(searchRequest).subscribe({
            next: (searchHotels: HotelDto[]) => subscriber.next({hotels: searchHotels}),
            complete: () => subscriber.complete()
        })
    )
}
