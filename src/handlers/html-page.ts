import 'source-map-support/register';
import {APIGatewayProxyEvent, APIGatewayProxyResult} from "aws-lambda";
import * as fs from "fs";

export const get = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    const data = fs.readFileSync(__dirname + '/../../assets/html/search.html', 'utf8')
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'text/html',
        },
        body: data,
    }
}
