import {SearchResponseDto} from "../classes/SearchResponseDto";
import SearchEnginesHandler from "../searchEngines/SearchEnginesHandler";
import SearchEngine from "../searchEngines/SearchEngine";
import {HotelDto} from "../classes/HotelDto";

export const searchRest = async (req, res) => {
    const searchRequest = req.body
    const searchEnginesHandler = new SearchEnginesHandler([
        new SearchEngine(),
        new SearchEngine(),
        new SearchEngine(),
        new SearchEngine()
    ])

    let hotels: HotelDto[] = []

    return new Promise((resolve) => {
        searchEnginesHandler.executeSearch(searchRequest).subscribe({
            next: (searchHotels: HotelDto[]) => hotels = hotels.concat(searchHotels),
            error: (e) => resolve({
                statusCode: 500,
                body: e.message
            }),
            complete: () => res.send(<SearchResponseDto>{hotels})
        })
    })
}
