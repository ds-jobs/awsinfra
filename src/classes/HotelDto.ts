export interface HotelDto {
    name: string;
    description: string;
    stars: number;
    address: string;
    price: number;
}