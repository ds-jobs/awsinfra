export default interface SearchRequestDto {
    where: string;
    fromDate: string;
    toDate: string;
}