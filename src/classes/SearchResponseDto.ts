import {HotelDto} from "./HotelDto";

export interface SearchResponseDto {
    hotels: HotelDto[]
}